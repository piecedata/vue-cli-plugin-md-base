module.exports = {
    parserOptions: {
        parser: 'babel-eslint',
    },

    env: {
        node: true,
    },

    extends: [
        'plugin:vue/recommended',
        'airbnb-base',
    ],

    plugins: [
        'vue',
    ],

    settings: {
        // 'import/resolver': {
        //     webpack: {
        //         config: './webpack.config.js',
        //     },
        // },
        'import/extensions': [
            '.js',
            '.jsx',
            '.mjs',
            '.ts',
            '.tsx',
            '.vue',
        ],
    },

    rules: {
        'max-len': 'warn',
        'no-shadow': 'off',
        'no-console': 'off',
        'global-require': 'off',
        'import/no-cycle': 'off', //?
        'no-param-reassign': 'off',
        'no-confusing-arrow': 'off',
        'no-underscore-dangle': 'off',
        'class-methods-use-this': 'off',
        'prefer-promise-reject-errors': 'off',
        'import/prefer-default-export': 'off',
        'no-debugger': process.env.NODE_ENV === 'production' ? 'error' : 'off',
        indent: [
            'error',
            2,
            {
                SwitchCase: 1,
                VariableDeclarator: 1,
                outerIIFEBody: 1,
                // MemberExpression: null,
                FunctionDeclaration: {
                    parameters: 1,
                    body: 1,
                },
                FunctionExpression: {
                    parameters: 1,
                    body: 1,
                },
                CallExpression: {
                    arguments: 1,
                },
                ArrayExpression: 1,
                ObjectExpression: 1,
                ImportDeclaration: 1,
                flatTernaryExpressions: false,
                // list derived from https://github.com/benjamn/ast-types/blob/HEAD/def/jsx.js
                ignoredNodes: [
                    'JSXElement',
                    'JSXElement > *',
                    'JSXAttribute',
                    'JSXIdentifier',
                    'JSXNamespacedName',
                    'JSXMemberExpression',
                    'JSXSpreadAttribute',
                    'JSXExpressionContainer',
                    'JSXOpeningElement',
                    'JSXClosingElement',
                    'JSXText',
                    'JSXEmptyExpression',
                    'JSXSpreadChild',
                ],
                ignoreComments: false,
            },
        ],
        semi: [
            'warn',
            'always',
        ],
        'comma-dangle': [
            'warn',
            'always-multiline',
        ],
        'import/extensions': [
            'error',
            'always',
            {
                js: 'never',
                mjs: 'never',
                jsx: 'never',
                ts: 'never',
                tsx: 'never',
                vue: 'never',
            },
        ],
        'object-shorthand': [
            'error',
            'always',
            {
                ignoreConstructors: false,
                avoidQuotes: false,
            },
        ],
        // vue
        'vue/html-indent': [
            'error',
            2,
            { alignAttributesVertically: false },
        ],
        'vue/max-attributes-per-line': [
            2,
            {
                singleline: 1,
                multiline: {
                    max: 1,
                    allowFirstLine: true,
                },
            },
        ],
        'vue/html-self-closing': [
            'error',
            {
                html: {
                    void: 'never',
                    normal: 'never',
                    component: 'always',
                },
                svg: 'always',
                math: 'always',
            },
        ],
        'vue/html-closing-bracket-newline': 'off',
        //     'no-unused-vars': [
        //         'warn',
        //         {
        //             vars: 'local',
        //             args: 'after-used',
        //             ignoreRestSiblings: true,
        //         },
        //     ],
        //     'space-before-function-paren': [
        //         'error',
        //         {
        //             anonymous: 'always',
        //             named: 'ignore',
        //             asyncArrow: 'always',
        //         },
        //     ],
        //     'prefer-const': 'warn',
        //     camelcase: [
        //         'error',
        //         { properties: 'never' },
        //     ],
    },
};
