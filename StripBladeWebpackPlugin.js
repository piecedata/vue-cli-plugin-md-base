class StripBladeWebpackPlugin {
    apply(compiler) {
        compiler.hooks.compilation.tap('StripBladeWebpackPlugin', (compilation) => {
            compilation
                .hooks
                .htmlWebpackPluginAfterHtmlProcessing
                .tapAsync('StripBladeWebpackPlugin', (pluginArgs, callback) => {
                    // const regex = /<!--\[if blade]>([\s\S]+?)<!\[endif]-->/g;
                    // const str = process.env.NODE_ENV === 'development' ? '' : '$1';

                    pluginArgs.html = pluginArgs.html
                        .replace(/{{[\s\S]+?}}/g, '')
                        .replace(/@include\([\s\S]+?\)/g, '');

                    callback(null, pluginArgs);
                });
        });
    }
}

module.exports = StripBladeWebpackPlugin;
