const merge = require('deepmerge');
const BrowserSyncPlugin = require('browser-sync-webpack-plugin');
const StripBladeWebpackPlugin = require('./StripBladeWebpackPlugin');

module.exports = (api, options) => {
    options.productionSourceMap = false;

    options.css = merge(options.css || {}, {
        loaderOptions: {
            sass: {
                data: '@import "@/styles/vars.scss";',
            },
            css: {
                localIdentName: process.env.NODE_ENV === 'production'
                    ? '[hash:base64:5]'
                    : '[path][name]-[local]-[hash:base64:5]',
                camelCase: true,
            },
        },
    });

    options.pwa = merge(options.pwa || {}, {
        appleMobileWebAppCapable: 'yes',
        appleMobileWebAppStatusBarStyle: 'black',
        iconPaths: {
            favicon32: 'favicon-32x32.png',
            favicon16: 'favicon-16x16.png',
            appleTouchIcon: 'apple-touch-icon.png',
            maskIcon: 'safari-pinned-tab.svg',
            msTileImage: 'mstile-150x150.png',
        },
    });

    api.chainWebpack((webpackConfig) => {
        webpackConfig
            .plugin('html')
            .tap((args) => {
                if (args[0]) {
                    Object.assign(args[0].minify || {}, {
                        removeAttributeQuotes: false,
                        processConditionalComments: true,
                    });
                }

                return args;
            });

        if (process.env.NODE_ENV === 'production') {
            webpackConfig
                .plugin('workbox')
                .tap((args) => {
                    args[0].exclude.push(/index\.html$/);

                    return args;
                });

            const minimizer = webpackConfig.optimization.store.get('minimizer');

            if (minimizer[0]) {
                const uglifyOptions = [
                    minimizer[0].options.uglifyOptions
                    || minimizer[0].options.terserOptions,
                    {
                        compress: {
                            arrows: true,
                            collapse_vars: true, // 0.3kb
                            comparisons: true,
                            computed_props: true,
                            hoist_funs: true,
                            hoist_props: true,
                            hoist_vars: true,
                            inline: true,
                            loops: true,
                            negate_iife: true,
                            properties: true,
                            reduce_funcs: true,
                            reduce_vars: true,
                            switches: true,
                            toplevel: true,
                            typeofs: true,
                        },
                    },
                ];

                if (options.pluginOptions
                    && options.pluginOptions.uglifyOptions) {
                    uglifyOptions.push(options.pluginOptions.uglifyOptions);
                }

                if (minimizer[0].options.uglifyOptions) {
                    minimizer[0].options.uglifyOptions = merge.all(uglifyOptions);
                } else if (minimizer[0].options.terserOptions) {
                    minimizer[0].options.terserOptions = merge.all(uglifyOptions);
                } else {
                    console.log('OPTIMIZATION ERROR!!!!!!!!!');
                }
            }
        } else {
            const browserSync = {
                proxy: 'http://localhost:8080',
                host: 'localhost',
                port: 3000,
                open: false,
            };

            if (options.pluginOptions
                && options.pluginOptions.browserSync) {
                Object.assign(browserSync, options.pluginOptions.browserSync);
            }

            webpackConfig
                .plugin('browser-sync')
                .use(BrowserSyncPlugin, [
                    browserSync,
                    { reload: false },
                ]);

            webpackConfig
                .plugin('StripBladeWebpackPlugin')
                .use(StripBladeWebpackPlugin);
        }
    });
};
